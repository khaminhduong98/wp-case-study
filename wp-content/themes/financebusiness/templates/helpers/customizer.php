<?php

/**
 * Theme Name: Finance Business Customizer Settings
 */

// WP_Customize_Control

function finbus_customizer_settings($wp_customizer) {

    // add a customizer setting
    // include new section
    $wp_customizer->add_section('service', array(
        'title' => __('Service Settings', 'Finance'),
        'priority' => 70
    ));

    // enable add capability to the customizer setting
    $wp_customizer->add_setting('service_image', array(
        'capability' => 'edit_theme_options'
    ));

    // add controls to the customizer settings
    $wp_customizer->add_control(new WP_Customize_Image_Control($wp_customizer, 'service_image', array(
        'label' => __('Service Image', 'Finance'),
        'section' => 'service'
    )));

    // add service text
    $wp_customizer->add_setting('service_text_field', array(
        'capability' => 'edit_theme_options',
    ));

    // add text control
    $wp_customizer->add_control('service_text_control', array(
        'label' => __('Service Text', 'Finance'),
        'description' => 'Adjust Service text',
        'section' => 'service',
        'settings' => 'service_text_field'
    ));

    // add service description
    $wp_customizer->add_setting('service_desc_field', array(
        'capability' => 'edit_theme_options',
        'default' => 'Aliquam id urna imperdiet libero mollis hendrerit'
    ));

    // add description control
    $wp_customizer->add_control('service_desc_control', array(
        'label' => __('Service Description', 'Finance'),
        'description' => 'Adjust Service description',
        'section' => 'service',
        'settings' => 'service_desc_field'
    ));
}

add_action('customize_register', 'finbus_customizer_settings');