<?php
/**
 * Template Name: Blog Posts
 */

// get blog posts
?>

<?php if ($wp_query->have_posts()) : ?>
<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

        <article class="blog-post col-md-4">
            <div class="service-item">
                <div class="post-thumbnail">
                    <?php
                        $thumbnail = '';
                        if ( has_post_thumbnail( get_the_ID() ) ) {
                        $thumbnail = the_post_thumbnail( get_the_ID(), 'thumbnail');
                        }
                        echo $thumbnail; ?>
                </div>
                <div class="post-info down-content">
                    <div class="post-title">
                        <h4><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h4>
                    </div>
                    <div class="post-author">
                        <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><span><?php the_author(); ?> <?php get_the_date(); ?>></span></a>
                    </div>
                    <div class="post-content">
                        <p><?php echo get_the_excerpt(); ?></p>
                        <a href="" class="filled-button">Read More</a>
                    </div>
                    <hr>
                    <div class="post-cat">
                        <?php get_template_part('templates/temp/post', 'categories'); ?>
                    </div>
                </div>
            </div>
        </article>

<?php endwhile; ?>
<?php else : ?>
<?php echo wpautop('Sorry, no post were found') ?>
<?php endif; ?>