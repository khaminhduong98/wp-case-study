<?php

$categories = get_the_category();
if (!empty($categories)) {
    foreach ($categories as $category) {
        printf('<a href="%1$s"><span>%2$s</span></a>',
        esc_url(get_category_link($category->term_id)),
        esc_html($category->name));
    }
}

?>

<!--<a href="#"><span>Commercial</span></a>-->
<!--<a href="#"><span>People</span></a>-->