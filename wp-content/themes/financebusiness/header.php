<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="TemplateMo">

    <!--    <title>Finance Business</title>-->

    <!-- Bootstrap core CSS -->
    <!--    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->

    <!-- Additional CSS Files -->
    <!--    <link rel="stylesheet" href="assets/css/fontawesome.css">-->
    <!--    <link rel="stylesheet" href="assets/css/templatemo-finance-business.css">-->
    <!--    <link rel="stylesheet" href="assets/css/owl.css">-->
<!--    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">-->
    <!--
    -->

    <?php wp_head(); ?>

</head>

<body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->

    <!-- Header -->
    <div class="sub-header">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <ul class="left-info">
                        <li><a href="#"><i class="fa fa-clock-o"></i>Mon-Fri 09:00-17:00</a></li>
                        <li><a href="#"><i class="fa fa-phone"></i>090-080-0760</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="right-icons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <header class="">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <a class="navbar-brand" href="index.html">
                    <h2 class="header-title">
                        <?php
                            if (function_exists('the_custom_logo')) {
                                the_custom_logo();
                            } else {
                                bloginfo('name');
                            }
                        ?>
                        Finance Business
                    </h2>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <?php
                        // primary menu
                        if (has_nav_menu('primary-menu')) {
                            wp_nav_menu(array(
                                'theme_location' => 'primary-menu',
                                'container' => '',
                                'items_wrap' => '<ul class="navbar-nav ml-auto">%3$s</ul>'
                            ));
                        }
                    ?>
<!--                    <ul class="navbar-nav ml-auto">-->
<!--                        <li class="nav-item active">-->
<!--                            <a class="nav-link" href="index.html">Home-->
<!--                                <span class="sr-only">(current)</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="nav-item">-->
<!--                            <a class="nav-link" href="about.html">About Us</a>-->
<!--                        </li>-->
<!--                        <li class="nav-item">-->
<!--                            <a class="nav-link" href="services.html">Our Services</a>-->
<!--                        </li>-->
<!--                        <li class="nav-item">-->
<!--                            <a class="nav-link" href="contact.html">Contact Us</a>-->
<!--                        </li>-->
<!--                    </ul>-->
                </div>
            </div>
        </nav>
    </header>

    <!-- Page Content -->
    <main>