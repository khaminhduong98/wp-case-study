<?php

/**
 *
 * Template Name: functions.php
 * Description: Finance Business functions and definitions
 *
 * @package Finance Business
 */

/**
 * Customizer additions.
 */
require_once('templates/helpers/customizer.php');

if (!function_exists('finbus_setup')) :

    function finbus_setup() {

        /**
         * manage document title <title> tag
         */
        add_theme_support('title-tag');

        /**
         * Post Thumbnails on posts and pages
         * add Featured image option
         */
        add_theme_support('post-thumbnails');

        /**
         * switch default core markup to output valid HTML5
         */
        add_theme_support('html5', array('search-form'));

        /**
         * custom logo functionality
         */
        add_theme_support('custom-logo');
    }

endif;
add_action('after_setup_theme', 'finbus_setup');

/**
 * remove admin bar for all users
 */
function remove_admin_bar() {
//    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
//    }
}

add_action('after_setup_theme', 'remove_admin_bar');

/**
 * include style files
 */
function finbus_styles() {

    // Google Fonts
    wp_enqueue_style('finbus-fonts', '//fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap');

    // Bootstrap
    wp_enqueue_style('bootstrap', get_template_directory_uri() .'/vendor/css/bootstrap.min.css');

    // Font Awesome
    wp_enqueue_style('fontawesome', get_template_directory_uri().'/vendor/css/fontawesome.css');

    // Flex Slider
    wp_enqueue_style('flex-slider', get_template_directory_uri().'/vendor/css/flex-slider.css');

    // Owl
    wp_enqueue_style('owl', get_template_directory_uri().'/vendor/css/owl.css');

    // Custom theme CSS
    wp_enqueue_style('custom-style', get_stylesheet_uri());

    wp_enqueue_style('media-responsive', get_template_directory_uri().'/assets/css/_media.css');
}

add_action('wp_enqueue_scripts', 'finbus_styles');

/**
 * include script files
 */
function finbus_scripts() {

    // Jquery
    wp_enqueue_script('jquery', get_template_directory_uri().'/vendor/js/jquery.min.js');

    // Bootstrap
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/vendor/js/bootstrap.bundle.min.js');

    // Custom theme JS
    wp_enqueue_script('custom-script', get_template_directory_uri().'/assets/js/custom.js');

    // Owl
    wp_enqueue_script('owl', get_template_directory_uri().'/vendor/js/owl.js');

    // Slick
    wp_enqueue_script('slick', get_template_directory_uri().'/vendor/js/slick.js');

    // Jquery UI
    wp_enqueue_script('accordions', get_template_directory_uri().'/vendor/js/accordions.js');

    // Util
    wp_enqueue_script('custom-script', get_template_directory_uri().'/assets/js/util.js');
}

add_action('wp_footer', 'finbus_scripts');

/**
 * Register navigation menu
 */
function finbus_nav_menu() {
    register_nav_menus(array(
        'primary-menu' => __('Primary Menu', 'text_domain'),
        'social-menu' => __('Social Menu', 'text_domain')
    ));
}

add_action('init', 'finbus_nav_menu');

// Add class to <a>
function add_link_attributes($attrs) {
    $attrs['class'] = 'nav-link';
    return $attrs;
}

add_filter('nav_menu_link_attributes', 'add_link_attributes');

// Active current nav item
function active_nav_class($classes, $item) {
    if (in_array('current-post-ancestor', $classes) || in_array('current-page-ancestor', $classes) || in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

add_filter('nav_menu_css_class', 'active_nav_class', 10, 2);

/**
 * Adjust excerpt length
 */
function finbus_custom_excerpt_length($length) {
    return 25;
}

add_filter('excerpt_length', 'finbus_custom_excerpt_length');