<!doctype html>
<html <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name') ?></title>

<!--    <link rel="stylesheet" href="--><?php //bloginfo('stylesheet_url') ?><!--">-->

    <?php wp_head(); ?>
</head>

<body <?php body_class('body-class'); ?>>

<nav>
    <?php
        if (has_nav_menu('header-menu')) {
            wp_nav_menu(array('theme_location' => 'header-menu'));
        }
    ?>
</nav>

<header class="header-area">
    <h1><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name');?></a></h1>
    <h3><?php bloginfo('description'); ?></h3>
</header>

<main id="main">